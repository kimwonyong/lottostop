(function() {
  // A low performance polyfill based on toDataURL.

  if (!HTMLCanvasElement.prototype.toBlob) {
    Object.defineProperty(HTMLCanvasElement.prototype, 'toBlob', {
      value: function(callback, type, quality) {

        var binStr = atob(this.toDataURL(type, quality).split(',')[1]),
          len = binStr.length,
          arr = new Uint8Array(len);

        for (var i = 0; i < len; i++) {
          arr[i] = binStr.charCodeAt(i);
        }

        callback(new Blob([arr], {
          type: type || 'image/png'
        }));
      }
    });
  }

  var app = new Vue({
    el: '#app',
    data: {
      local: '',
      localText: '',
      localArr: [],
      pano: null,
      isSearch: false,
      recommendArr: [],
      numberArr: [],
      getLocation: {},
      title: null,
      tempTitle: sharedata.title,
      resultHistory: JSON.parse(localStorage.getItem('resultHistory')) || [],
      resultModalObj: {},
      isDraw: false,
      storageRef: null,
      isLoading: false
    },
    computed: {
      groupByResult: function() {
        var that = this;
        return _.groupBy(that.resultHistory, function(o) {
          return moment(o.time, 'YYYY.MM.DD').format('YYYY.MM.DD');
        });
      }
    },
    mounted: function() {
      var that = this;
      that.panoInit();
      that.recommendInit();

      $('.button-collapse').sideNav();
      $('#modal-result').modal({
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5, // Opacity of modal background
        inDuration: 300, // Transition in duration
        outDuration: 200, // Transition out duration
        startingTop: '10%', // Starting top style attribute
        endingTop: '10%', // Ending top style attribute
      });

      $('.carousel.carousel-slider').carousel({
        fullWidth: true
      });

      var config = {
        apiKey: "AIzaSyCMW9gjUsCD58t63dkXlcwttPrWAKV5UWc",
        authDomain: "lottostop-69b9f.firebaseapp.com",
        databaseURL: "https://lottostop-69b9f.firebaseio.com",
        projectId: "lottostop-69b9f",
        storageBucket: "lottostop-69b9f.appspot.com",
        messagingSenderId: "1042743343157"
      };

      firebase.initializeApp(config);
      // Get a reference to the storage service, which is used to create references in your storage bucket
      var storage = firebase.storage();
      var storageRef = storage.ref();
      that.storageRef = storageRef;

      Kakao.init('64e2f8265f9af03341b63b5853a8bc68');
      if (sharedata.numberArr) {
        sharedata.numberArr.forEach(function(item, index) {
          var color;
          if (item <= 10) {
            color = '#fbc02d';
          } else if (item <= 20) {
            color = '#0288d1';
          } else if (item <= 30) {
            color = '#d32f2f';
          } else if (item <= 40) {
            color = '#616161';
          } else {
            color = '#388e3c';
          }
          sharedata.numberArr[index] = {
            num: item,
            color: color
          };
        });

        sharedata.numberArr = that.sortedNumberArr(sharedata.numberArr);

        that.historyLocal(sharedata);
      }
    },
    methods: {
      btnShare: function(resultModalObj) {
        var that = this;
      },
      genUuid: function() {
        function s4() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
          s4() + '-' + s4() + s4() + s4();
      },
      recommendRefresh: function() {
        var that = this;
        that.recommendInit();
      },
      recommendInit: function() {
        var that = this;
        $.ajax({
            method: 'GET',
            url: '/recommend'
          })
          .done(function(res) {
            that.recommendArr = res;
          });
      },
      deleteHistory: function(uuid) {
        var that = this;
        that.resultHistory = that.resultHistory.filter(function(o) {
          return o.uuid !== uuid;
        });
        localStorage.setItem('resultHistory', JSON.stringify(that.resultHistory));
        Materialize.toast('선택한 번호를 삭제했습니다.', 4000);
      },
      resetHistory: function() {
        var that = this;
        localStorage.clear();
        that.resultHistory = [];
        Materialize.toast('저장된 번호를 초기화했습니다.', 4000);
      },
      sortedNumberArr: function(numberArr) {
        var that = this;
        return _.sortBy(numberArr, ['num']);
      },
      momentFormat: function(time, format) {
        var that = this;
        return moment(time, format).format(format);
      },
      formatedYear: function(time) {
        var that = this;
        var year = moment(time, 'YYYY').format('YYYY');
        if (year == '2017') {
          year = '정유(丁酉)년';
        } else {
          year = '무술(戊戌)년';
        }
        return year;
      },
      historyLocal: function(item) {
        var that = this;
        that.pano.setPanoId(item.panoId);
        that.tempTitle = item.title;
        that.itemActive(item);

        that.resultModalObj = item;


        history.pushState(null, null, '/' + that.momentFormat(item.time, 'YYYYMMDDHH') + '/' + encodeURIComponent(item.panoId));

        $('#modal-result').modal('open');
      },
      drawNumber: function() {
        var that = this;
        var time = moment();
        that.getLocation = that.pano.getLocation();
        var reusltExist = _.find(that.resultHistory, function(o) {
          return (that.momentFormat(o.time, 'YYYYMMDDHH') === that.momentFormat(time, 'YYYYMMDDHH')) && o.panoId === that.pano.getPanoId();
        });


        if (reusltExist) {
          $('ul.tabs').tabs('select_tab', 'tab2');
          that.historyLocal(reusltExist);
          Materialize.toast('같은 장소, 같은 시간에 뽑은 번호가 있습니다. <br>다른 장소, 다른 시간에 뽑아주세요.', 4000);
        } else {
          that.isDraw = true;
          that.isLoading = true;
          $.ajax({
              method: 'GET',
              url: '/lotto',
              data: {
                title: that.title,
                panoId: that.pano.getPanoId(),
                address: that.getLocation.address
              }
            })
            .done(function(res) {

              res.numberArr.forEach(function(item, index) {
                var color;
                if (item <= 10) {
                  color = '#fbc02d';
                } else if (item <= 20) {
                  color = '#0288d1';
                } else if (item <= 30) {
                  color = '#d32f2f';
                } else if (item <= 40) {
                  color = '#616161';
                } else {
                  color = '#388e3c';
                }
                res.numberArr[index] = {
                  num: item,
                  color: color
                };
              });

              that.resultModalObj = res;
              that.resultModalObj.uuid = that.genUuid();
              that.resultModalObj.numberArr = that.sortedNumberArr(that.resultModalObj.numberArr);

              $('#modal-result').css({
                'display': 'block',
                'z-index': -1
              });
              var node = $('#modal-result').find('.modal-content')
              var filename = 'lottoNum/' + that.momentFormat(res.time, 'YYYYMMDDHH') + ':' + encodeURIComponent(res.panoId);
              var imagesRef = that.storageRef.child(filename);

              imagesRef.getDownloadURL().then(function(url) {
                showNumber();
              }, function(err) {
                html2canvas(node, {
                  onrendered: function(canvas) {
                    canvas.toBlob(function(blob) {
                      imagesRef.put(blob, {
                        contentType: 'image/jpeg'
                      }).then(function(snapshot) {
                        showNumber();
                      });
                    });
                  }
                });
              });

              function showNumber() {
                that.isLoading = false;
                that.numberArr = [];
                res.numberArr.forEach(function(item, index) {
                  that.numberArr.push({
                    num: item.num,
                    isShow: false,
                    color: item.color
                  });

                  setTimeout(function() {
                    that.numberArr[index]['isShow'] = true;
                  }, 1000 * index);

                  setTimeout(function() {
                    that.numberArr[index]['isShow'] = false;
                    if (index === (that.numberArr.length - 1)) {
                      that.resultHistory.unshift(that.resultModalObj);
                      localStorage.setItem('resultHistory', JSON.stringify(that.resultHistory));
                      setTimeout(function() {
                        $(location).attr('href', '/' + that.momentFormat(res.time, 'YYYYMMDDHH') + '/' + encodeURIComponent(res.panoId));
                      }, 500);
                    }
                  }, 1000 * index + 500);
                });
              }
            });
        }
      },
      panoInit: function() {
        var that = this;
        var panoResize = _.debounce(function() {
          that.pano.setSize(new naver.maps.Size($('main').width(), $('main').height()));
        }, 100);

        that.pano = new naver.maps.Panorama('pano', {
          panoId: sharedata.panoId,
          zoomControl: true,
          zoomControlOptions: {
            position: naver.maps.Position.TOP_RIGHT,
            style: naver.maps.ZoomControlStyle.LARGE
          },
          aroundControl: true,
          aroundControlOptions: {
            position: naver.maps.Position.TOP_RIGHT
          }
        });

        $(window).resize(function() {
          panoResize();
        });

        naver.maps.Event.addListener(that.pano, 'pano_status', function(status) {
          if (status === 'ERROR') {
            Materialize.toast('죄송합니다. 해당지역은 파라노마를 지원하지 않습니다.', 4000);
          } else {
            that.title = that.tempTitle;
          }
        });
      },
      recommendLocal: function(item) {
        var that = this;
        that.pano.setPanoId(item.panoId);
        that.tempTitle = item.title;

        that.itemActive(item);
      },
      searchLocal: _.debounce(function(local) {
        var that = this;
        if (local) {
          that.localText = local;
          that.isSearch = true;
          $.ajax({
              method: 'GET',
              url: '/local',
              data: {
                local: local
              }
            })
            .done(function(res) {
              that.localArr = res.items;
              that.localArr[0] && that.setCenter(that.localArr[0]);
              $('ul.tabs').tabs('select_tab', 'tab1');
            });
        }
      }, 500),
      setCenter: function(item) {
        var that = this;
        var tm128 = new naver.maps.Point(item.mapx, item.mapy);
        var latLng = naver.maps.TransCoord.fromTM128ToLatLng(tm128);
        that.tempTitle = item.title;
        that.pano.setPosition(latLng);
        that.itemActive(item);
      },
      itemActive: function(item) {
        var that = this;
        that.localArr = that.localArr.map(function(o) {
          o.isActive = false;
          return o;
        });

        that.recommendArr = that.recommendArr.map(function(o) {
          o.isActive = false;
          return o;
        });

        that.resultHistory = that.resultHistory.map(function(o) {
          o.isActive = false;
          return o;
        });

        item.isActive = true;
      }
    }
  });
})();
