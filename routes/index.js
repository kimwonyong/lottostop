var express = require('express');
var router = express.Router();
var _ = require('lodash');
var moment = require('moment');
var request = require('request');
var striptags = require('striptags');
var CronJob = require('cron').CronJob;
var async = require('async');
var admin = require('firebase-admin');
var client_id = '05DY7RHB92zFoCNRuOfy';
var client_secret = 'Zm7zkPA2fK';
var lottoNumArr = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
  21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45
];
var chineseZodiacArr = [{
  animal: '쥐',
  name: '자',
  chinese: '子',
  description: '쥐가 제일 열심히 뛰어 다니는 때.',
  time: ['23', '00']
}, {
  animal: '소',
  name: '축',
  chinese: '丑',
  description: '밤새 풀을 먹은 소가 한참 반추하며 아침 밭갈이 준비를 할 때.',
  time: ['01', '02']
}, {
  animal: '호랑이',
  name: '인',
  chinese: '寅',
  description: '하루 중 호랑이가 제일 흉악한 때.',
  time: ['03', '04']
}, {
  animal: '토끼',
  name: '묘',
  chinese: '卯',
  description: '해뜨기 직전에 달이 아직 중천에 걸려 있어 그 속에 옥토끼가 보이는 때.',
  time: ['05', '06']
}, {
  animal: '용',
  name: '진',
  chinese: '辰',
  description: '용이 날면서 강우 준비를 하는 때.',
  time: ['07', '08']
}, {
  animal: '뱀',
  name: '사',
  chinese: '巳',
  description: '이 시간에 뱀은 자고 있어 사람을 해치는 일이 없는 때.',
  time: ['09', '10']
}, {
  animal: '말',
  name: '오',
  chinese: '午',
  description: '이 시간에는 고조에 달했던 ‘양기’가 점점 기세를 죽이며 ‘음기’가 머리를 들기 시작하는데, 말은 땅에서 달리고 땅은 ‘음기’이므로 말을 ‘음기’의 동물로 보고 이 시각을 말과 연계시킨다.',
  time: ['11', '12']
}, {
  animal: '양',
  name: '미',
  chinese: '未',
  description: '양이 이때 풀을 뜯어 먹어야 풀이 재생하는 데 해가 없다.',
  time: ['13', '14']
}, {
  animal: '원숭이',
  name: '신',
  chinese: '申',
  description: '이 시간에 원숭이가 울음소리를 제일 많이 낸다.',
  time: ['15', '16']
}, {
  animal: '닭',
  name: '유',
  chinese: '酉',
  description: '하루 종일 모이를 쫓던 닭이 둥지에 들어가는 때.',
  time: ['17', '18']
}, {
  animal: '개',
  name: '술',
  chinese: '戌',
  description: '날이 어두워지니 개가 집을 지키기 시작하는 때.',
  time: ['19', '20']
}, {
  animal: '돼지',
  name: '해',
  chinese: '亥',
  description: '돼지가 단잠을 자고 있을 때.',
  time: ['21', '22']
}];


var serviceAccount = require('../firebase/serviceAccountKey.json');
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://lottostop-69b9f.firebaseio.com'
});
var db = admin.database();

var recommendArr = [];
var getRecommend = function() {
  var ref = db.ref('lottoNum').limitToLast(100);
  ref.once('value', function(snapshot) {
    var listObj = snapshot.val();
    var listArr = _.map(listObj);
    recommendArr = _.uniqBy(listArr, 'panoId');
  });
}
getRecommend();

new CronJob('00 00 00 * * *', function() {
  getRecommend();
}, null, true);



var gcloud = require('gcloud')({
  projectId: 'lottostop-69b9f',
  keyFilename: 'firebase/serviceAccountKey.json'

});
var gcs = gcloud.storage();
var bucket = gcs.bucket('lottostop-69b9f.appspot.com');








router.get('/', function(req, res) {
  res.render('index', {
    title: '로또스탑',
    data: {
      title: '독도',
      panoId: 'GF4+zemn5jQMUIau657mxQ=='
    },
    image: 'http://lottostop.co.kr/img/lottostop.png',
    url: 'http://lottostop.co.kr/',
    description: '풍수지리 패턴 분석(Feng shui Pattern Analysis)을 기반으로 로또예상번호를 무료로 제공합니다.'
  });
});

// 풍수지리 패턴 분석 API
router.get('/:time/:panoid', function(req, res) {
  var time = req.params.time;
  var panoid = req.params.panoid;
  var key = 'lottoNum/' + time + ':' + encodeURIComponent(panoid);
  async.series([
    function(callback) {
      var ref = db.ref(key);
      ref.once('value', function(snapshot) {
        // firebase 저장되어 있으면,
        if (snapshot.val()) {
          callback(null, snapshot.val());
        } else {
          callback(204);
        }
      });
    },
    function(callback) {
      bucket.file(key).getSignedUrl({
        action: 'read',
        expires: '12-31-9999'
      }, function(err, url) {
        if (err) {
          callback(null, 'http://lottostop.co.kr/img/lottostop.png');
        } else {
          request.get(url, function(err, httpResponse, body) {
            if (httpResponse.statusCode === 200) {
              callback(null, url);
            } else {
              callback(null, 'http://lottostop.co.kr/img/lottostop.png');
            }
          });
        }
      });
    }
  ], function(err, result) {
    if (err) {
      res.redirect('/');
    } else {
      var year = moment(result[0].time).format('YYYY');
      if (year == '2017') {
        year = '정유(丁酉)년';
      } else {
        year = '무술(戊戌)년';
      }

      res.render('index', {
        title: '로또스탑 - ' + _.sortBy(result[0].numberArr),
        data: result[0],
        image: result[1],
        url: 'http://lottostop.co.kr/' + time + '/' + encodeURIComponent(panoid),
        description: year + ' ' + result[0].chineseZodiac.name + '(' + result[0].chineseZodiac.chinese + ')시 ' + result[0].title + '에서 뽑은 예상번호는 ' + _.sortBy(result[0].numberArr) + ' 입니다.'
      });
    }
  });
});



// 네이버 local 검색 API
router.get('/local', function(req, res) {
  var local = req.query.local;
  local = encodeURI(local);
  var api_url = 'https://openapi.naver.com/v1/search/local.json?query=' + local + '&display=10&start=1&sort=random';

  var options = {
    url: api_url,
    headers: {
      'X-Naver-Client-Id': client_id,
      'X-Naver-Client-Secret': client_secret
    }
  };
  request.get(options, function(error, response, body) {
    body = JSON.parse(body);
    if (!error && response.statusCode === 200) {
      body.items.forEach(function(item) {
        item.title = striptags(item.title);
      });
      res.status(200).json(body);
    } else {
      res.status(response.statusCode).json(body);
    }
  });
});

// 풍수지리 패턴 분석 API
router.get('/lotto', function(req, res) {
  var panoId = req.query.panoId;
  var title = req.query.title;
  var address = req.query.address;
  var now = moment();
  var time = now.format('YYYYMMDDHH');
  var ref = db.ref('lottoNum/' + time + ':' + encodeURIComponent(panoId));

  var chineseZodiac = _.find(chineseZodiacArr, function(o) {
    return o.time.indexOf(now.format('HH')) > -1
  });

  ref.once('value', function(snapshot) {
    // firebase 저장되어 있으면,
    if (snapshot.val()) {
      res.status(200).json(snapshot.val());
    } else {
      var numberArr = _.sampleSize(lottoNumArr, 6);
      var result = {
        title: title,
        panoId: panoId,
        address: address,
        time: now.format('YYYY.MM.DD HH:mm:ss'),
        numberArr: numberArr,
        chineseZodiac: chineseZodiac
      };
      ref.set(result);
      res.status(200).json(result);
    }
  });
});





// 추천 장소 API
router.get('/recommend', function(req, res) {
  var result = _.sampleSize(recommendArr, 3);
  res.status(200).json(result);
});



module.exports = router;
